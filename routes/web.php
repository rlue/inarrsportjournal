<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('home');
Route::get('/premierlanguage','LeagueController@premierlanguage')->name('premierlanguage');
Route::get('/bundesliga','LeagueController@bundesliga')->name('bundesliga');
Route::get('/serieA','LeagueController@serieA')->name('serieA');
Route::get('/ligueOne','LeagueController@ligueOne')->name('ligueOne');
Route::get('/spainlaliga','LeagueController@spainlaliga')->name('spainlaliga');
Route::get('/championship','LeagueController@championship')->name('championship');
Route::get('/topscorers','HomeController@topscorers')->name('topscorers');
Route::get('/news','PageController@news')->name('news');
Route::get('/articles','PageController@articles')->name('articles');
Route::get('/forecast','PageController@forecast')->name('forecast');
Route::get('/results','PageController@results')->name('results');
Route::get('/myanmarnews','PageController@myanmarnews')->name('myanmarnews');
Route::get('/worldcup','PageController@worldcup')->name('worldcup');
Route::get('/contactus','PageController@contactus')->name('contactus');
Route::get('/detail/{id}','PageController@detail')->name('detail');

Route::get('/getarea','LeagueController@index');

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/backend/login','Auth\LoginController@showLoginForm')->name('login');
Route::post('/backend/login','Auth\LoginController@login');
Route::post('/backend/logout','Auth\LoginController@logout')->name('logout');
Route::group(['middleware' => 'auth', 'prefix' => 'backend'], function () {
	

	
	Route::get("users", 'Admin\UserController@index')->name("users.index");
	Route::get("users/create", 'Admin\UserController@create')->name("users.create");
	Route::get("users/{id}/edit", 'Admin\UserController@edit')->name("users.edit");
	Route::get("users/delete/{id}", 'Admin\UserController@destroy')->name("users.delete");
	Route::patch('users/update/{id}',"Admin\UserController@update")->name('users.update');
	Route::post('users/store', 'Admin\UserController@store')->name("users.store");


	//role route
	Route::get("roles", 'Admin\RoleController@index')->name("roles.index");
	Route::get("roles/create", 'Admin\RoleController@create')->name("roles.create");
	Route::get("roles/{id}/edit", 'Admin\RoleController@edit')->name("roles.edit");
	Route::get("roles/delete/{id}", 'Admin\RoleController@destroy')->name("roles.delete");
	Route::patch('roles/update/{id}',"Admin\RoleController@update")->name('roles.update');
	Route::post('roles/store', 'Admin\RoleController@store')->name("roles.store");

	//Permission route
	Route::get("permissions", 'Admin\PermissionController@index')->name("permissions.index");
	Route::get("permissions/create", 'Admin\PermissionController@create')->name("permissions.create");
	Route::get("permissions/{id}/edit", 'Admin\PermissionController@edit')->name("permissions.edit");
	Route::get("permissions/delete/{id}", 'Admin\PermissionController@destroy')->name("permissions.delete");
	Route::patch('permissions/update/{id}',"Admin\PermissionController@update")->name('permissions.update');
	Route::post('permissions/store', 'Admin\PermissionController@store')->name("permissions.store");

	// category route
	Route::get("category", 'Admin\CategoryController@index')->name("category.index");
	// Tag route
	Route::get("league", 'Admin\TagController@index')->name("tag.index");
	// TopScores route
	Route::get("topscorers", 'Admin\TopScoreController@index')->name("topscores.index");
	
	// news
	Route::get("news", 'Admin\NewController@index')->name("news.index");
	
	// Ads route
	Route::get("ads", 'Admin\AdsController@index')->name("ads.index");
	

	Route::group(['middleware' => ['permission:update']], function () {
    //

		//for news
		Route::get("news/{id}/edit", 'Admin\NewController@edit')->name("news.edit");
		Route::patch('news/update/{id}',"Admin\NewController@update")->name('news.update');
		//for category
		Route::get("category/{id}/edit", 'Admin\CategoryController@edit')->name("category.edit");
		Route::patch('category/update/{id}',"Admin\CategoryController@update")->name('category.update');
		//for tag
		Route::get("league/{id}/edit", 'Admin\TagController@edit')->name("tag.edit");
		Route::patch('league/update/{id}',"Admin\TagController@update")->name('tag.update');
		//for TopScores
		Route::get("topscorers/{id}/edit", 'Admin\TopScoreController@edit')->name("topscores.edit");
		Route::patch('topscorers/update/{id}',"Admin\TopScoreController@update")->name('topscores.update');
		//for ads
		Route::get("ads/{id}/edit", 'Admin\AdsController@edit')->name("ads.edit");
		Route::patch('ads/update/{id}',"Admin\AdsController@update")->name('ads.update');
	});

	Route::group(['middleware' => ['permission:delete']],function(){
		Route::get("ads/delete/{id}", 'Admin\AdsController@destroy')->name("ads.delete");
		Route::get("category/delete/{id}", 'Admin\CategoryController@destroy')->name("category.delete");
		Route::get("topscorers/delete/{id}", 'Admin\TopScoreController@destroy')->name("topscores.delete");
		Route::get("league/delete/{id}", 'Admin\TagController@destroy')->name("tag.delete");
		Route::get("news/delete/{id}", 'Admin\NewController@destroy')->name("news.delete");
	});
	
	Route::group(['middleware' => ['permission:insert']],function(){
		Route::get("category/create", 'Admin\CategoryController@create')->name("category.create");
		Route::post('category/store', 'Admin\CategoryController@store')->name("category.store");
		Route::get("league/create", 'Admin\TagController@create')->name("tag.create");
		Route::post('league/store', 'Admin\TagController@store')->name("tag.store");
		Route::get("topscorers/create", 'Admin\TopScoreController@create')->name("topscores.create");
		Route::post('topscorers/store', 'Admin\TopScoreController@store')->name("topscores.store");
		Route::get("ads/create", 'Admin\AdsController@create')->name("ads.create");
		Route::post('ads/store', 'Admin\AdsController@store')->name("ads.store");
		Route::get("news/create", 'Admin\NewController@create')->name("news.create");
		Route::post('news/store', 'Admin\NewController@store')->name("news.store");
	});
});

Route::get('instant_news/rss', [
    'as' => 'app.new.rss',
    'uses' => 'NewController@rss',
]);

Route::get('instant_news/{slug}', [
    'as' => 'app.blog.view',
    'uses' => 'NewController@instant_view',
]);
