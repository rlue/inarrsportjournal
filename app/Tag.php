<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
     protected $fillable = [
        'tag_name'
    ];
    protected $primaryKey = 'tag_id';
}
