<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopScore extends Model
{
    //
    protected $table = 'top_scorers';
    protected $fillable = [
        'topscores_name','topscores_image','topscores_team','topscores_goal','topscores_league'
    ];
    protected $primaryKey = 'topscores_id';
}
