<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Ads extends Model
{
    //
    use HasRoles;
    
     protected $fillable = [
        'ads_title','ads_position','ads_image','ads_url'
    ];
}
