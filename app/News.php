<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    protected $dates = ['published_at'];
    //
     protected $fillable = [
        'new_title','new_description','feature_image','categoryid','published_at','status'
    ];
    protected $primaryKey = 'new_id';

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', now());
    }

    /**
     * @param Builder $query
     * @return mixed
     */
    public function scopeEnabled($query)
    {
        return $query->where('status', self::STATUS_ENABLED);
    }
}
