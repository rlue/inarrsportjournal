<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTopScores extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'topscores_name'  => 'required',
            'topscores_image'  => 'required',
            'topscores_team'  => 'required',
            'topscores_goal'  => 'required|integer',
            'topscores_league'  => 'required'
        ];
    }
}
