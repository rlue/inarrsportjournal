<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
class PageController extends Controller
{
    //

    public function news()
    {
    	$pagetitle = "News | Inarr Sports Journal";
    	$page = "News";
    	$adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        //for side bar lasted news
        $getnew = News::where('categoryid','=','2')->orderBy('new_id','desc')->limit(4)->get();
    	$pages = News::where('categoryid','=',2)->orderBy('new_id','desc')->paginate(20);
    	$breakingnew = $this->breakingnew();
    	return view('page.catpage',compact('pages','adstop','adsbanner','bannerleft','bannerleft','adsfooter','getnew','pagetitle','page','breakingnew'));
    }
    public function articles()
    {
    	$pagetitle = "Aritlces | Inarr Sports Journal";
    	$page = "Articles";
    	 $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
       //for side bar lasted news
        $getnew = News::where('categoryid','=','2')->orderBy('new_id','desc')->limit(4)->get();
    	$pages = News::where('categoryid','=',3)->orderBy('new_id','desc')->paginate(20);
    	$breakingnew = $this->breakingnew();
    	return view('page.catpage',compact('pages','adstop','adsbanner','bannerleft','adsfooter','getnew','pagetitle','page','breakingnew'));
    }
    public function forecast()
    {
    	$pagetitle = "Forecast | Inarr Sports Journal";
    	$page = "Forecast";
    	 $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        //for side bar lasted news
        $getnew = News::where('categoryid','=','2')->orderBy('new_id','desc')->limit(4)->get();
    	$pages = News::where('categoryid','=',4)->orderBy('new_id','desc')->paginate(20);
    	$breakingnew = $this->breakingnew();
    	return view('page.catpage',compact('pages','adstop','adsbanner','bannerleft','adsfooter','getnew','pagetitle','page','breakingnew'));
    }
    public function results()
    {
    	$pagetitle = "Result | Inarr Sports Journal";
    	$page = "Result";
    	 $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        //for side bar lasted news
        $getnew = News::where('categoryid','=','2')->orderBy('new_id','desc')->limit(4)->get();
    	$pages = News::where('categoryid','=',5)->orderBy('new_id','desc')->paginate(20);
    	$breakingnew = $this->breakingnew();
    	return view('page.catpage',compact('pages','adstop','adsbanner','bannerleft','adsfooter','getnew','pagetitle','page','breakingnew'));
    }
    public function worldcup()
    {
    	$pagetitle = "World Cup | Inarr Sports Journal";
    	$page = "World Cup";
    	 $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        //for side bar lasted news
        $getnew = News::where('categoryid','=','2')->orderBy('new_id','desc')->limit(4)->get();
    	$pages = News::where('categoryid','=',6)->orderBy('new_id','desc')->paginate(20);
    	$breakingnew = $this->breakingnew();
    	return view('page.catpage',compact('pages','adstop','adsbanner','bannerleft','adsfooter','getnew','pagetitle','page','breakingnew'));
    }
    public function myanmarnews()
    {
       $pagetitle = "Myanmar Sports News | Inarr Sports Journal";
        $page = "Myanmar Sports News";
         $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        //for side bar lasted news
        $getnew = News::where('categoryid','=','2')->orderBy('new_id','desc')->limit(4)->get();
        $pages = News::where('categoryid','=',7)->orderBy('new_id','desc')->paginate(20);
        $breakingnew = $this->breakingnew();
        return view('page.myanmarnews',compact('pages','adstop','adsbanner','bannerleft','adsfooter','getnew','pagetitle','page','breakingnew')); 
    }
    public function detail($id)
    {
    	$pagetitle = "Detail | Inarr Sports Journal";
    	$page = "Detail";
    	 $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        //for side bar lasted news
        $getnew = News::where('categoryid','=','2')->orderBy('new_id','desc')->limit(4)->get();
    	$detail = News::FindOrFail($id);
    	$catid = $detail->categoryid;
    	$related = News::where('categoryid','=',$catid)->limit(3)->orderBy('new_id','desc')->get();
    	$breakingnew = $this->breakingnew();
    	return view('page.detail',compact('detail','adstop','adsbanner','bannerleft','adsfooter','getnew','pagetitle','page','related','breakingnew'));
    }
    public function contactus()
    {
        $pagetitle = "Contact Us | Inarr Sports Journal";
        $page = "Contact Us";
         $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
       
        $breakingnew = $this->breakingnew();
        return view('page.contactus',compact('adstop','adsbanner','bannerleft','adsfooter','pagetitle','page','breakingnew'));
    }
}
