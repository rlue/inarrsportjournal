<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Tag;
use App\Topscore;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagetitle = "Home | Inarr Sports Journal";
        $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        $breakingnew = $this->breakingnew();
        $slide = News::where('categoryid','=','2')
        ->orWhere('categoryid','=','3')
        ->orWhere('categoryid','=','4')
        ->orWhere('categoryid','=','5')
        ->orWhere('categoryid','=','6')
        ->orderBy('new_id','desc')->limit(4)->get();
        //for side bar news
        $getnew = News::where('categoryid','=','2')->orderBy('new_id','desc')->limit(4)->get();
        //for home page news
        $news = News::where('categoryid','=','3')
        ->orWhere('categoryid','=','4')
        ->orWhere('categoryid','=','5')
        ->orWhere('categoryid','=','6')
        ->orderBy('new_id','desc')->limit(3)->get();
        return view('page.home',compact('news','getnew','adstop','adsfooter','adsbanner','bannerleft','pagetitle','breakingnew','slide'));
    }
    public function topscorers()
    {
        $pagetitle = "Top Scorers | Inarr Sports Journal";
         $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        $breakingnew = $this->breakingnew();
       //for right side bar
        $getnew = News::where('categoryid','=','2')->orderBy('new_id','desc')->limit(4)->get();
         $league = Tag::get();
        $premier = DB::table('top_scorers')
        ->leftJoin('tags', 'top_scorers.topscores_league', '=', 'tags.tag_id')
        ->where('topscores_league','1')
        ->orderBy('topscores_goal','desc')
        ->get();
        $laliga = DB::table('top_scorers')
        ->leftJoin('tags', 'top_scorers.topscores_league', '=', 'tags.tag_id')
        ->where('topscores_league','2')
        ->orderBy('topscores_goal','desc')
        ->get();
        $seriea = DB::table('top_scorers')
        ->leftJoin('tags', 'top_scorers.topscores_league', '=', 'tags.tag_id')
        ->where('topscores_league','3')
        ->orderBy('topscores_goal','desc')
        ->get();
        $bundesliga = DB::table('top_scorers')
        ->leftJoin('tags', 'top_scorers.topscores_league', '=', 'tags.tag_id')
        ->where('topscores_league','4')
        ->orderBy('topscores_goal','desc')
        ->get();
         $franceLigue = DB::table('top_scorers')
        ->leftJoin('tags', 'top_scorers.topscores_league', '=', 'tags.tag_id')
        ->where('topscores_league','6')
        ->orderBy('topscores_goal','desc')
        ->get();
         
        return view('page.topscorers',compact('premier','laliga','seriea','bundesliga','franceLigue','league','pagetitle','getnew','adstop','adsfooter','adsbanner','bannerleft','pagetitle','breakingnew'));
    }
}
