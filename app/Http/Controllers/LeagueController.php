<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Football;
use App\News;
use GuzzleHttp\Client as GuzzleClient;
class LeagueController extends Controller
{
    //
    
    public function premierlanguage()
    {
        $filter = '2021';
       
    	
        
        $headers = [
            'X-Auth-Token' => 'ce929f1db84541e697fbf2853635deca',
        ];

        $client = new GuzzleClient([
            'headers' => $headers
        ]);

       

        $r = $client->request('GET', 'http://api.football-data.org/v2/competitions/2021/standings');
        $response = $r->getBody()->getContents();
        $reste = json_decode($response);

        $premier = $reste->standings['0'];
        $pagetitle = "Premier Language | Inarr Sports Journal";
    	$adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        $getnew = News::orderBy('new_id','desc')->limit(4)->get();
        $breakingnew = $this->breakingnew();
    	return view('page.leaguetable',compact('pagetitle','premier','getnew','adstop','adsbanner','bannerleft','adsfooter','breakingnew'));
    }
     public function bundesliga()
    {
    	$headers = [
            'X-Auth-Token' => 'ce929f1db84541e697fbf2853635deca',
        ];

        $client = new GuzzleClient([
            'headers' => $headers
        ]);

       

        $r = $client->request('GET', 'http://api.football-data.org/v2/competitions/2002/standings');
        $response = $r->getBody()->getContents();
        $reste = json_decode($response);

        $premier = $reste->standings['0'];

    	 $pagetitle = "Bundesliga | Inarr Sports Journal";
        $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        $getnew = News::orderBy('new_id','desc')->limit(4)->get();
        $breakingnew = $this->breakingnew();
    	return view('page.bundesliga',compact('pagetitle','premier','getnew','adstop','adsbanner','bannerleft','adsfooter','breakingnew'));
    }
     public function serieA()
    {
    	$headers = [
            'X-Auth-Token' => 'ce929f1db84541e697fbf2853635deca',
        ];

        $client = new GuzzleClient([
            'headers' => $headers
        ]);

       

        $r = $client->request('GET', 'http://api.football-data.org/v2/competitions/2019/standings');
        $response = $r->getBody()->getContents();
        $reste = json_decode($response);

        $premier = $reste->standings['0'];

    	 $pagetitle = "Serie A | Inarr Sports Journal";
        $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        $getnew = News::orderBy('new_id','desc')->limit(4)->get();
        $breakingnew = $this->breakingnew();
    	return view('page.serieA',compact('pagetitle','premier','getnew','adstop','adsbanner','bannerleft','adsfooter','breakingnew'));
    }
     public function ligueOne()
    {
    	$headers = [
            'X-Auth-Token' => 'ce929f1db84541e697fbf2853635deca',
        ];

        $client = new GuzzleClient([
            'headers' => $headers
        ]);

       

        $r = $client->request('GET', 'http://api.football-data.org/v2/competitions/2015/standings');
        $response = $r->getBody()->getContents();
        $reste = json_decode($response);

        $premier = $reste->standings['0'];

    	 $pagetitle = "Ligue 1 | Inarr Sports Journal";
        $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        $getnew = News::orderBy('new_id','desc')->limit(4)->get();
        $breakingnew = $this->breakingnew();
    	return view('page.ligueOne',compact('pagetitle','premier','getnew','adstop','adsbanner','bannerleft','adsfooter','breakingnew'));
    }
     public function spainlaliga()
    {
    	$headers = [
            'X-Auth-Token' => 'ce929f1db84541e697fbf2853635deca',
        ];

        $client = new GuzzleClient([
            'headers' => $headers
        ]);

       

        $r = $client->request('GET', 'http://api.football-data.org/v2/competitions/2014/standings');
        $response = $r->getBody()->getContents();
        $reste = json_decode($response);

        $premier = $reste->standings['0'];

    	 $pagetitle = "Laliga | Inarr Sports Journal";
        $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        $getnew = News::orderBy('new_id','desc')->limit(4)->get();
        $breakingnew = $this->breakingnew();
    	return view('page.spainlaliga',compact('pagetitle','premier','getnew','adstop','adsbanner','bannerleft','adsfooter','breakingnew'));
    }
     public function championship()
    {
        $headers = [
            'X-Auth-Token' => 'ce929f1db84541e697fbf2853635deca',
        ];

        $client = new GuzzleClient([
            'headers' => $headers
        ]);

       

        $r = $client->request('GET', 'http://api.football-data.org/v2/competitions/2016/standings');
        $response = $r->getBody()->getContents();
        $reste = json_decode($response);

        $premier = $reste->standings['0'];

         $pagetitle = "Championship | Inarr Sports Journal";
        $adstop = $this->adstop();
        if (!$adstop->isEmpty())
        {
            $adstop = $adstop->random(1);
        }
        $adsbanner = $this->adsbanner();
        if (!$adsbanner->isEmpty())
        {
            $adsbanner = $adsbanner->random(1);
        }
        $bannerleft = $this->adsbanner_left();
         if (!$bannerleft->isEmpty())
        {
            $bannerleft = $bannerleft->random(1);
        }
        $adsfooter = $this->adsfooter();
        if (!$adsfooter->isEmpty())
        {
            $adsfooter = $adsfooter->random(1);
        }
        $getnew = News::orderBy('new_id','desc')->limit(4)->get();
        $breakingnew = $this->breakingnew();
        return view('page.championship',compact('pagetitle','premier','getnew','adstop','adsbanner','bannerleft','adsfooter','breakingnew'));
    }
}
