<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Ads;
use App\News;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function adstop()
    {
    	return $adstop = Ads::where('ads_position', '=','top')->get();
	}
	 public function adsfooter()
    {
    	return $adsfooter = Ads::where('ads_position', '=','footer')->get();
	}
	 public function adsbanner()
    {
    	return $adsbanner = Ads::where('ads_position', '=','page_top')->get();
	}
	 public function adsbanner_left()
    {
    	return $leftbanner = Ads::where('ads_position', '=','banner_v')->get();
	}
    public function breakingnew()
    {
        return $breaking = News::where('categoryid','=','2')->orWhere('categoryid','=','7')->orderBy('new_id','desc')->limit(5)->get();
    }
}
