<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Category;
use App\Http\Requests\StoreNew;
use DB;
use Session;
use Facebook\InstantArticles\Client\Client;
use Illuminate\Support\Str;
use Guid;
class NewController extends Controller
{
   
    public function rss()
    {
        $blogs = News::enabled()
            ->published()
            ->latest('published_at')
            ->get();

        $blogs->map(function ($each) {
            if (empty($each->guid)) {
                $each->guid = Guid::create();
            }
        });

        return view('rss', [
            'blogs' => $blogs,
            'title' => 'Inarr Weekly Sport Journal',
        ]);
    }

    public function instant_view($slug=null)
    {
        return News::where('new_id', $slug)->first();
    }
}
