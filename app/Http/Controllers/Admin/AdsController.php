<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use App\Ads;
use Session;
use DB;
class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
       
        $ads = Ads::get();
        return view('admin.ads.index',compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
        $adsposition = config('adsposition');
        return view('admin.ads.create',compact('adsposition'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       
        $data['ads_title']   = $request->input('ads_title');
        $data['ads_position'] = $request->input('ads_position');
        $data['ads_url'] = $request->input('ads_url');
        $data['ads_image'] = $request->input('ads_image');
         
       
         $media = Ads::create($data);
       Session::flash('message', 'You have successfully Insert Ads.');
        return redirect()->route("ads.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
        $ads = Ads::FindOrFail($id);

        
        $adsposition =  config('adsposition');
        return view('admin.ads.edit',compact('ads','adsposition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $data['ads_title']   = $request->input('ads_title');
        $data['ads_position'] = $request->input('ads_position');
        $data['ads_url'] = $request->input('ads_url');
        $data['ads_image'] = $request->input('ads_image');

         Ads::findOrFail($id)->update($data);
       Session::flash('message', 'You have successfully Update Ads.');
        return redirect()->route("ads.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        Ads::destroy($id);
       
        Session::flash('message', 'You have successfully deleted Ads.');
        return redirect()->route("ads.index"); 
    }
}
