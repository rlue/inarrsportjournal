<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Category;
use App\Http\Requests\StoreNew;
use DB;
use Session;
use Facebook\InstantArticles\Client\Client;
class NewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $new = DB::table('news')
        ->leftJoin('categories', 'news.categoryid', '=', 'categories.category_id')
        ->orderBy('new_id','desc')
        ->get();
        return view('admin.news.index',compact('new'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $category = Category::get();
        return view('admin.news.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNew $request)
    {
        //
        $input=[];
        $input=$request->all();
        $input['published_at']=date('Y-m-d');
        News::create($input);
         
       Session::flash('message', 'You have successfully Insert News.');
        return redirect()->route("news.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Category::get();
         $news = News::FindOrFail($id);
        return view('admin.news.edit',compact('news','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input=[];
        $input = $request->all();
        $input['published_at'] = date('Y-m-d');
       
        
        News::findOrFail($id)->update($input);
       
        Session::flash('message', 'You have successfully updated News.');
        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         News::destroy($id);

        Session::flash('message', 'You have successfully Delete news.');
        return redirect()->route("news.index");
    }

   
}
