<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Permission;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use DB;
use Session;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware("auth");
        $this->middleware("superauth");
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()->paginate();
        return view('admin.user.index', compact('users'));
    }
    public function create()
    {
        $roles = Role::get();        
        return view('admin.user.create', compact('roles'));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'roles' => 'required'
        ]);
         $data = [
        'name'      => $request->input("name"),
        'email'     => $request->input('email'),
        'password'  => bcrypt($request->input('password')),
        
        ];
        $user = User::create($data);
        
        if($request->roles <> ''){
            $user->roles()->attach($request->roles);
        }
        return redirect()->route('users.index')->with('success','User has been created');            
        
    }
    public function edit($id) {
        $user = User::findOrFail($id);
        $roles = Role::get(); 
        return view('admin.user.edit', compact('user', 'roles')); 
    }
    public function update(Request $request, $id) {
        $user = User::findOrFail($id);   
        $this->validate($request, [
            
            'password'=>'confirmed'
        ]);
        $input = $request->except('roles');
        
         $name  = $request->input("name");
        $email     = $request->input('email');
        $password  = $request->input('password');
        
        if(isset($password) && !empty($password)){
            $userupdate = ['name' => $name,'email' => $email,'password'=>bcrypt($password)];

        }else{
             $userupdate = ['name' => $name,'email' => $email];
        }
       
       DB::table('users')
            ->where('id', $id)
            ->update($userupdate);
        if ($request->roles <> '') {
            $user->roles()->sync($request->roles);        
        }        
        else {
            $user->roles()->detach(); 
        }
        return redirect()->route('users.index')->with('success',
             'User successfully updated.');
    }
    public function destroy($id) {
        $user = User::findOrFail($id); 
        $user->delete();
        return redirect()->route('users.index')->with('success',
             'User successfully deleted.');
    }
}
