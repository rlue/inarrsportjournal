<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TopScore;
use App\Tag;
use App\Http\Requests\StoreTopScores;
use DB;
use Session;

class TopScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $league = Tag::get();
        $topscore = DB::table('top_scorers')
        ->leftJoin('tags', 'top_scorers.topscores_league', '=', 'tags.tag_id')
        ->get();
        return view('admin.topscores.index',compact('topscore','league'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $league = Tag::get();
        return view('admin.topscores.create',compact('league'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTopScores $request)
    {
        //

         TopScore::create($request->all());

       Session::flash('message', 'You have successfully Insert News.');
        return redirect()->route("topscores.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $league = Tag::get();
         $topscores = TopScore::FindOrFail($id);
        return view('admin.topscores.edit',compact('topscores','league'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        TopScore::findOrFail($id)->update($request->all());
       
        Session::flash('message', 'You have successfully updated TopScores.');
        return redirect()->route('topscores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         TopScore::destroy($id);

        Session::flash('message', 'You have successfully Delete topscore.');
        return redirect()->route("topscores.index");
    }
}
