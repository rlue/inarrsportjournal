<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopScorersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_scorers', function (Blueprint $table) {
            $table->increments('topscores_id');
            $table->string('topscores_name');
            $table->string('topscores_image');
            $table->string('topscores_team');
            $table->integer('topscores_goal');
            $table->integer('topscores_league');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('top_scorers');
    }
}
