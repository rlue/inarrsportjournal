
@extends('layouts.main')

@section('style')
<style type="text/css">
#leag_1,#leag_2{
	background-color:#445ac7;
	color:#fff;
}
#leag_3,#leag_4,#leag_5,#leag_6{
	background-color:#969225;
}

#leag_22,#leag_23,#leag_24{
	background-color:#ef2b34;
	color:#fff;
}
.champion_btn{
	background: #445ac7;
	padding:5px;
}
.europa_btn{
	background: #969225;
	padding:5px;
}
.europaqu_btn{
	background: #d8d462;
	padding:5px;
}
.relegation_btn{
	background: #ef2b34;
	padding:5px;
}
</style>
@stop
@section('content')

	<div class="row">
				<div class="col-sm-8">
					<h3>ChampionShip England</h3>
					<div class="table-responsive">
					<table class="table table-border">
						<tr>
							<th>#</th>
							<th>Logo</th>
							<th>TeamName</th>
							<th>Played</th>
							<th>W</th>
							<th>D</th>
							<th>L</th>
							<th>Goals</th>
							<th title="GoalsAgainst">GA</th>
							<th title="GoalDifference" >GD</th>
							<th>Points</th>
						</tr>
						@foreach($premier->table as $pre)
						
						<tr id="leag_{{$pre->position}}">
							<td>{{$pre->position}}</td>
							<td><img src="{{$pre->team->crestUrl}}" width="20" /></td>
							<td>{{$pre->team->name}}</td>
							<td>{{$pre->playedGames}}</td>
							<td>{{$pre->won}}</td>
							<td>{{$pre->draw}}</td>
							<td>{{$pre->lost}}</td>
							<td>{{$pre->goalsFor}}</td>
							<td>{{$pre->goalsAgainst}}</td>
							<td>{{$pre->goalDifference}}</td>
							<td>{{$pre->points}}</td>
						</tr>
						@endforeach
					</table>	
					</div>
					<hr/>
					<p>
				 <button class="champion_btn"></button>
				  <span>Promotion</span>
				 </p>
				 <p>
				 <button class="europa_btn"></button>
				  <span>Promotion play-off</span>
				 </p>
				
				 <p>
				 <button class="relegation_btn"></button>
				  <span>Relegation </span>
				 </p>
				</div>
				<div class="col-sm-4">
					@include('nav.homeside')
				</div>
			</div>

@endsection