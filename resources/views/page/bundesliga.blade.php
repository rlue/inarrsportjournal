
@extends('layouts.main')

@section('style')
<style type="text/css">
#leag_1,#leag_2,#leag_3,#leag_4{
	background-color:#445ac7;
	color:#fff;
}
#leag_5{
	background-color:#969225;
}
#leag_6{
	background-color:#d8d462;
}
#leag_16{
	background-color:#ff7279;
	color:#fff;
}
#leag_17,#leag_18{
	background-color:#ef2b34;
	color:#fff;
}
.champion_btn{
	background: #445ac7;
	padding:5px;
}
.europa_btn{
	background: #969225;
	padding:5px;
}
.europaqu_btn{
	background: #d8d462;
	padding:5px;
}
.relegationqu_btn{
	background: #ff7279;
	padding:5px;
}
.relegation_btn{
	background: #ef2b34;
	padding:5px;
}
</style>
@stop
@section('content')

	<div class="row">
				<div class="col-lg-8">
					<h3>Bundesliga </h3>
					<div class="table-responsive">
					<table class="table table-border">
						<tr>
							<th>#</th>
							<th>Logo</th>
							<th>TeamName</th>
							<th>Played</th>
							<th>W</th>
							<th>D</th>
							<th>L</th>
							<th>Goals</th>
							<th title="GoalsAgainst">GA</th>
							<th title="GoalDifference" >GD</th>
							<th >Points</th>
						</tr>
						@foreach($premier->table as $pre)
						
						<tr id="leag_{{$pre->position}}">
							<td>{{$pre->position}}</td>
							<td><img src="{{$pre->team->crestUrl}}" width="20" /></td>
							<td>{{$pre->team->name}}</td>
							<td>{{$pre->playedGames}}</td>
							<td>{{$pre->won}}</td>
							<td>{{$pre->draw}}</td>
							<td>{{$pre->lost}}</td>
							<td>{{$pre->goalsFor}}</td>
							<td>{{$pre->goalsAgainst}}</td>
							<td>{{$pre->goalDifference}}</td>
							<td>{{$pre->points}}</td>
						</tr>
						@endforeach
					</table>	
					</div>
					<hr/>
					<p>
				 <button class="champion_btn"></button>
				  <span>Champions League</span>
				 </p>
				 <p>
				 <button class="europa_btn"></button>
				  <span>Europa  League</span>
				 </p>
				  <p>
				 <button class="europaqu_btn"></button>
				  <span>Europa League qualification</span>
				 </p>
				 <p>
				 <button class="relegation_btn"></button>
				  <span>Relegation </span>
				 </p>
				 <p>
				 <button class="relegationqu_btn"></button>
				  <span>Relegation play-off </span>
				 </p>
				</div>
				<div class="col-lg-4">
					<div class="side_bar_news">
						<section class="side_lastest_news">
							<h3>Latest News</h3>
						</section>
						<br>
						<a href="#">
						<div class="side_lastest_news2">
							<img class="side_img" src="images/news_pic(1).jpg">
							<br><br>
							<a href="#" class="main_title">ခ်န္ပီယံလိဂ ဒုတိယအေက်ာ့ ပြဲစဥ္တြင္ ေနမာ ပါဝင္မကားစားႏုင္သျဖင့္ စိမေကာင္းျဖစ္မိမည္ဟု ရီးရဲမက္ဒရစ္ နည္းျပ ဇီဒန္း ဖြင့္ဟ</a>
						</div>
						</a>
						@foreach($getnew as $new)
						<div class="row side_latest_new_title">
							<div class="news_img">
								<img src="{{$new->feature_image}}" class="news_logo">	
							</div>
							
							<div class="news_title">
								<a href="#" class="main_title">{{$new->new_title}}</a>
							</div>
						</div>
						@endforeach
						
					</div>

					<div class="row">
						<div class="side_ads">
							<img src="images/side_ads.jpg" class="side_ads_img">
						</div>
					</div>

					@include('nav.standing')
				</div>
			</div>

@endsection