@extends('layouts.main')

@section('content')

	<div class="row">
				<div class="col-sm-8">

					<h2 class="related_title">{{$page}}</h2><br>

					<div class="row">
					@if(!$pages->isEmpty())
						@foreach($pages as $cat)
						<div class="col-sm-3 up_list">
							<div class="list">
								<a href="{{route('detail',$cat->new_id)}}"><img class="related_img" src="{{$cat->feature_image}}"></a><br><br>
								<a href="{{route('detail',$cat->new_id)}}" class="cat_title">{!! str_limit($cat->new_title, $limit = 100, $end = '...Read More') !!}</a>
							</div>
						</div>
						@endforeach
						<div class="col-sm-12">
						{{$pages->links()}}
						</div>
					@else
					<div class="jumbotron">
					  <h1>Sorry!</h1>
					  <p>This page is empty data</p>
					  
					</div>
					
						
					@endif
						@include('nav.footerads')
					</div>
				</div>
				<div class="col-sm-4">
					@include('nav.homeside')
				</div>
			</div>

@endsection