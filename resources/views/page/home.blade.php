
@extends('layouts.main')


@section('content')
<!-- <div id="loader"></div> -->
<div  class="animate-bottom">

	<div class="row">
				<div class="col-sm-8">
					<div class="row main_col">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
					  <!-- Indicators -->
					  <ol class="carousel-indicators">
					  <?php $i = 0; ?>
					  @foreach($slide as $s)
					  	@if($i == 0)
					    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active"></li>
					    @else
					    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>"></li>
					    @endif
					   
					    <?php $i++; ?>
					   @endforeach
					  </ol>

					  <!-- Wrapper for slides -->
					  <div class="carousel-inner">
					  <?php $l = 0; ?>
					  @foreach($slide as $s)
					  @if($l == 0)
					  	<a href="{{route('detail',$s->new_id)}}">
					    <div class="item active">
					      <img class="lastest_news" src="{{$s->feature_image}}">
									<div class="main_news_title">
										<a href="{{route('detail',$s->new_id)}}" class="main_title_link">{{$s->new_title}}</a>
									</div>
					    </div>
					    </a>
					    @else
					    <a href="{{route('detail',$s->new_id)}}">
					    <div class="item">
					      <img class="lastest_news" src="{{$s->feature_image}}">
									<div class="main_news_title">
										<a href="{{route('detail',$s->new_id)}}" class="main_title_link">{{$s->new_title}}</a>
									</div>
					    </div>
					    </a>
					    @endif
					    <?php $l++; ?>
					   @endforeach
					    
					  </div>

					</div>			
						
					</div>
					
					<br>
					<div class="col-sm-4 mobile_view">ss
					@include('nav.homeside')
					</div>
					<br>

					@include('nav.bannerads')
					<br>
					@foreach($news as $n)
					<a href="#">
					<div class="row main_news">
						<div class="col-sm-5 main_news_pic">
							<a href="{{route('detail',$n->new_id)}}">
							<img class="main_img" src="{{$n->feature_image}}"></img>
							</a>
						</div>
						<div class="col-sm-7">
							<a href="{{route('detail',$n->new_id)}}" class="main_title"><h3>{{$n->new_title}}</h3></a>
							<div class="win_home">{!! str_limit($n->new_description, $limit = 150, $end = '...') !!}<a class="mmfont read_more" href="{{route('detail',$n->new_id)}}">Read More</a></div>
						</div>
					</div>
					</a>
					@endforeach
					
					@include('nav.footerads')	
				</div>
				<div class="col-sm-4 web_view">
					@include('nav.homeside')
				</div>
			</div>

@endsection