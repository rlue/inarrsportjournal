@extends('layouts.main')


@section('detail_id')
    {{$detail->new_id}}
@endsection

@section('title')
    {{ $detail->new_title}}
@endsection


@section('content')

	<div class="row">
				<div class="col-sm-8">
					@include('nav.bannerads')
				<h2 class="related_title">{{$page}}</h2>
				<br>
				<h3 class="mmfont">{{$detail->new_title}}</h3>
			<br>
					@if($detail->feature_image)
						<img class="lastest_news" src="{{$detail->feature_image}}">
					@else

					@endif
										
					
					<div class="news_body">{!! $detail->new_description !!}</div>
					<br>
					<div class="col-sm-4 mobile_view">
					@include('nav.homeside')
					</div>
					
					<br>
					@include('nav.footerads')
					<br>
					<div class="related_news">
					<h4 class="related_title">RELATED NEWS</h4><br>
					<div class="row">
					@foreach($related as $r)
						<div class="col-sm-4">
							<a href="{{route('detail',$r->new_id)}}"><img class="related_img" src="{{$r->feature_image}}"></a><br><br>
							<a href="{{route('detail',$r->new_id)}}" class="main_title">{{$r->new_title}}</a><br><br>
						</div>
					@endforeach	
					</div>
				</div>
				</div>
				<div class="col-sm-4 web_view">
					@include('nav.homeside')
				</div>
			</div>

@endsection