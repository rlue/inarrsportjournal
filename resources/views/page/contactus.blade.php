@extends('layouts.main')

@section('content')

	<div class="row">
			<h3>{{$page}}</h3>
				<div class="col-lg-5">
				<iframe class="googelmap" src="https://www.google.com/maps/d/embed?mid=1ofP_svG3JNM9ObqH4d5X6qp0zaWrBrnZ"></iframe>
			</div>
			<div class="col-lg-7">
				<h2>Inarr Sport Journal</h2>
				<table class="table tbl">
					<tr>
						<td><span class="fa fa-phone"></span> Phone:</td>
						<td>01256621,09964256621</td>
					</tr>
					<tr>
						<td><span class="fa fa-map-marker"></span> Address:</td>
						<td>167/173, Room B2, Seikkanthar Yeikmon, Seikkanthar Street (middle block),Kyauktada Township , Yangon</td>
					</tr>
					<tr>
						<td><span class="fa fa-envelope"></span> Mail:</td>
						<td>inarr.journal@gmail.com</td>
					</tr>
					
				</table>
			</div>
			</div>

@endsection