<div id="loader"></div>
<div style="display:none;" id="myDiv" class="animate-bottom">
@extends('layouts.main')

@section('content')

	<div class="row">
				<div class="col-sm-8">
					

					@include('nav.bannerads')
					<br>
          <h3>Top Scorers</h3>
          <br>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Premier League
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body table-responsive">
        <table class="table">
						<tr class="th_tbl">
							<th>Name</th>
							<th>Team</th>
							<th>Goals</th>
						</tr>
						@foreach($premier as $top)
						<tr class="th_tbl">
							<td>
                <div class="title_img">
                  <img width="50" src="{{$top->topscores_image}}" />
                  <p>{{$top->topscores_name}}</p>
                </div>
							</td>
							<td>{{$top->topscores_team}}</td>
							<td>{{$top->topscores_goal}}</td>
						</tr>
						@endforeach
					</table>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          La Liga
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body table-responsive">
         <table class="table">
            <tr class="th_tbl">
              <th>Name</th>
              <th>Team</th>
              <th>Goals</th>
            </tr>
            @foreach($laliga as $l)
            <tr class="th_tbl">
              <td>
                <div class="title_img">
                  <img width="50" src="{{$l->topscores_image}}" />
                  <p>{{$l->topscores_name}}</p>
                </div>
              </td>
              <td>{{$l->topscores_team}}</td>
              <td>{{$l->topscores_goal}}</td>
            </tr>
            @endforeach
          </table>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Serie A
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body table-responsive">
         <table class="table">
            <tr class="th_tbl">
              <th>Name</th>
              <th>Team</th>
              <th>Goals</th>
            </tr>
            @foreach($seriea as $s)
            <tr class="th_tbl">
              <td>
                <div class="title_img">
                  <img width="50" src="{{$s->topscores_image}}" />
                  <p>{{$s->topscores_name}}</p>
                </div>
              </td>
              <td>{{$s->topscores_team}}</td>
              <td>{{$s->topscores_goal}}</td>
            </tr>
            @endforeach
          </table>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          Bundesliga
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
      <div class="panel-body table-responsive">
        <table class="table">
            <tr class="th_tbl">
              <th>Name</th>
              <th>Team</th>
              <th>Goals</th>
            </tr>
            @foreach($bundesliga as $b)
            <tr class="th_tbl">
              <td>
                <div class="title_img">
                  <img width="50" src="{{$b->topscores_image}}" />
                  <p>{{$b->topscores_name}}</p>
                </div>
              </td>
              <td>{{$b->topscores_team}}</td>
              <td>{{$b->topscores_goal}}</td>
            </tr>
            @endforeach
          </table>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFive">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          France Ligue 1
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
      <div class="panel-body table-responsive">
         <table class="table">
            <tr class="th_tbl">
              <th>Name</th>
              <th>Team</th>
              <th>Goals</th>
            </tr>
            @foreach($franceLigue as $f)
            <tr class="th_tbl">
              <td>
                <div class="title_img">
                  <img width="50" src="{{$f->topscores_image}}" />
                  <p>{{$f->topscores_name}}</p>
                </div>
              </td>
              <td>{{$f->topscores_team}}</td>
              <td>{{$f->topscores_goal}}</td>
            </tr>
            @endforeach
          </table>
      </div>
    </div>
  </div>
 
</div>
					
					
					
					@include('nav.footerads')	
				</div>
				<div class="col-sm-4">
					@include('nav.homeside')
				</div>
			</div>

@endsection