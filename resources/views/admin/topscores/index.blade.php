@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

                <h3>TopScorers Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Manage <small>TopScorers</small></h2>

                    @can('insert')
                    <a href="{{route('topscores.create')}}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create TopScorers</a>
                    @endcan
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="row">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-4">
                     <select name="" id="dropdown1" class="form-control">
                      <option value>All</option>
                   @foreach($league as $l)
                    <option value="{{$l->tag_name}}">{{$l->tag_name}}</option>
                   @endforeach
                 </select>
                 </div>
                 </div>
                 <div class="col-sm-4"></div>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Image</th>
                          <th>Name</th>
                          <th>Team</th>
                    
                          <th>Goals</th>
                          <th>League</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($topscore as $c)
                        <tr>
                          <td> <img width="200" height="100" src="{{ URL::to('/') }}{{$c->topscores_image}}" /></td>
                          <td>{{$c->topscores_name}}</td>
                          <td>{{$c->topscores_team}}</td>
                         
                          <td>{{$c->topscores_goal}}</td>
                          <td>{{$c->tag_name}}</td>
                         
                          <td>
                          @can('update')
                          <a href="{{route('topscores.edit',$c->topscores_id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
                          @endcan
                          </td>
                          <td>
                          @can('delete')
                          <a href="{{route('topscores.delete',$c->topscores_id)}}" class="btn btn-danger removed"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
                          @endcan
                          </td>
                          
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
          </div>
        </div>
@endsection


@section('footerscript')
  
   <script type="text/javascript">
     $(document).ready(function(){
      var table =  $('#datatable').DataTable();
    
              $('#dropdown1').on('change', function () {
                var ss = $("#dropdown1 option:selected").text();

                    if(ss != 'All'){
                      table.columns(5).search(ss).draw();
                    }else{
                      table.columns(5).search($(this).val()).draw();
                    }
                    
                } );
              
              
     });
   </script>
  
      
@stop