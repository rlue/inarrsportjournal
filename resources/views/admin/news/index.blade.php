@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

                <h3>News Manage<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>Manage <small>News</small></h2>
                    @can('insert')
                    <a href="{{route('news.create')}}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create News</a>
                    @endcan
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Catgory</th>
                          <th>Image</th>
                          
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($new as $c)
                        <tr>
                          <td>{{$c->new_id}}</td>
                          <td>{{$c->new_title}}</td>
                          <td class="table_desc">
                            {!! str_limit(strip_tags(htmlspecialchars_decode($c->new_description)), $limit = 50, $end = '...') !!}
                          </td>
                          <td>{{$c->category_name}}</td>
                          <td> 
                            <img width="200" height="100" src="{{ URL::to('/') }}{{$c->feature_image}}" />
                          </td>
                           
                          <td>
                            @can('update')
                            <a href="{{route('news.edit',$c->new_id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
                            @endcan
                          </td>
                          <td>
                            @can('delete')
                            <a href="{{route('news.delete',$c->new_id)}}" class="btn btn-danger removed"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
                            @endcan
                          </td>
                          
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
          </div>
        </div>
@endsection


