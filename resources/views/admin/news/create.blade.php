@extends('layouts.admin')

@section('content')
    

   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>News Manage</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>News Insert Form <small>different form elements</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                     @if (count($errors) > 0)
       <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
                    <form method="post" action="{{ route('news.store') }}">
                        {{ csrf_field() }}
                      
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class='form-control' id="new_title" name="new_title" placeholder="Title">
                        </div>
                        <div class="form-group">
                            <label for="title">Description</label>
                            <textarea name="new_description" id="summernote" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                        <label for="title">Feature Image</label>
                                                 <div class="input-group">
                        
                           <span class="input-group-btn">
                             <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                               <i class="fa fa-picture-o"></i> Choose
                             </a>
                           </span>
                           <input id="thumbnail" id="feature_image" class="form-control" type="text" name="feature_image">
                         </div>
                         <div class="form-group">
                         <img id="holder" style="margin-top:15px;max-height:100px;">
                          </div>
                        </div>
                       <div class="form-group">
                          <label for="title">Category</label>
                          <select name="categoryid" id="categoryid" class="form-control">
                              @foreach($category as $p)
                              <option value="{{$p->category_id}}">{{$p->category_name}}</option>
                            @endforeach
                          </select>
                       </div>
                       <div class="form-group">
                          <label for="title">Instant Article</label>
                          <label class="radio-inline">
                            <input type="radio" name="status" id="inlineRadio1"  value="0"> Not Use
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="status" id="inlineRadio2" checked value="1"> Use
                          </label>
                       </div>
                        <div class="form-group">

                            <a href="{{route('news.index')}}" class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel</a>
                            <button type="submit" id="newSave" class="btn btn-primary"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Save</button>
                        </div>
                   </form> 
                  </div>
                </div>
              </div>
            </div>

           
         
          </div>
        </div>
@endsection


