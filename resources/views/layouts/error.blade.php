<!DOCTYPE html>
<html>
<head>
	<title>404 Error page | Sport New Journal in Myanmar</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	@include('nav.header')
	@yield('style')
</head>
<body>
	
	
	
		<div class="container">
			
			<!-- logo -->
			<!-- Menu -->
				
	<div class="row">
		<div class="col-sm-4">
			<a href="{{route('home')}}"><img src="{{ asset('images/logo.jpg') }}" class="logo_img"></a>
		</div>
		<div class="col-sm-8">

		
		</div>
	</div>
   <div class="row">
	   <div class="col-sm-12">
			<!-- Menu -->
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      	<a href="#" class="navbar-brand">
	               		Menu
	                </a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li  class="@if(Request::segment(1) != 'news' AND Request::segment(1) != 'articles' AND Request::segment(1) != 'forecast' AND Request::segment(1) != 'results' AND Request::segment(1) != 'worldcup' AND Request::segment(1) != 'contactup' ) active @endif"><a href="{{route('home')}}">HOME <span class="sr-only">(current)</span></a></li>
			        <li  class="@if(Request::segment(1) == 'news'
			        ) active @endif"><a href="{{route('news')}}">NEWS</a></li>
			        <li  class="@if(Request::segment(1) == 'articles' ) active @endif"><a href="{{route('articles')}}">ARTICLES</a></li>
			        <li  class="@if(Request::segment(1) == 'forecast') active @endif"><a href="{{route('forecast')}}">FORECAST</a></li>
			        <li  class="@if(Request::segment(1) == 'results' ) active @endif"><a href="{{route('results')}}">RESULTS</a></li>
			        <li  class="@if(Request::segment(1) == 'worldcup') active @endif"><a href="{{route('worldcup')}}">WORLD CUP</a></li>
			        <li><a href="{{route('news')}}">CONTACT US</a></li>
			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
			<!-- logo and Menu -->
		</div>
	</div>
			<!-- logo and Menu -->
		@yield('content')
			<br>
			
		</div>
	


	<!-- Footer -->
	<div class="row footer">
		<div class="col-sm-8"><p class="pull-left moblie_foot">Copy Right © 2018 Inarr Sport Journal in Myanmar. All rights reserved.</p>
			
		</div>
		<div class="col-sm-4"><p class="pull-right moblie_foot">Develop By iTech Co,Ltd.</p>
		</div>
	</div>
	<!-- End of Footer -->
	


</body>
</html>