<!DOCTYPE html>
<html>
<head>
	<title>{{$pagetitle}}</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta property="fb:pages" content="105401581079653" />
    @if(isset($detail))
    <meta property="og:url" content="{{route('detail',$detail->new_id)}}" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{$detail->new_title}}" />
    <meta property="og:description" content="{{$detail->new_title}}" />
    <meta property="og:image" content="{{url('/').$detail->feature_image}}">
    @endif
	@include('nav.header')
	@yield('style')
</head>
<body onload="myFunction()" style="margin:0;">


	@include('nav.breaking')
	
		<div class="container">
			
			<!-- logo -->
			<!-- Menu -->
			@include('nav.logo')
			<!-- logo and Menu -->
		@yield('content')
			<br>
			
		</div>
	


	<!-- Footer -->
	<div class="row footer">
		<div class="col-sm-8"><p class="pull-left moblie_foot">Copy Right © 2018 Inarr Sports Journal in Myanmar. All rights reserved.</p>
			
		</div>
		<div class="col-sm-4"><p class="pull-right moblie_foot">Develop By iTech Co,Ltd.</p>
		</div>
	</div>
	<!-- End of Footer -->
</div>
<script>
var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116907449-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-116907449-1');
</script>

</body>
</html>
