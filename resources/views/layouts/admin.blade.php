<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>InArr Journal | Sports News Myanmar </title>

    <!-- Bootstrap -->

    
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
     
     <link href="{{ asset('css/nprogress.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
   <!-- NProgress -->
   <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adminstyle.css') }}" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"> <span>InArr Journal</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                
              </div>
              <div class="profile_info">
              
                <span>Welcome,</span>
                <h2>{{ Auth::user()->name }}</h2>
             
                
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                 
                  <li><a><i class="fa fa-edit"></i> User Manage <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      
                      <li><a href="{{route('users.index')}}">User List</a></li>
                      <li><a href="{{route('roles.index')}}">Role List</a></li>
                      <li><a href="{{route('permissions.index')}}">Permission List</a></li>
                    </ul>
                  </li>
                 
                  <li><a><i class="fa fa-edit"></i> News Manage <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('news.index')}}">News List</a></li>
                      <li><a href="{{route('category.index')}}">Category List</a></li>
                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> TopScores Manage <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{route('topscores.index')}}">TopScores List</a></li>
                      <li><a href="{{route('tag.index')}}">League List</a></li>
                      
                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> ADS Manage <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('ads.index')}}">Ads List</a></li>
                      
                      
                    </ul>
                  </li>
                </ul>
              </div>
             

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <!-- <a id="menu_toggle"><i class="fa fa-bars"></i></a> -->
              </div>
                @if (Auth::guest())
                  <h2>Login</h2>
              @else
                 <ul class="nav navbar-nav navbar-right">
                <li class="">

                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    
                    {{ Auth::user()->name }}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
                  </ul>
                </li>

                
              </ul>
              @endif
             
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        

         @yield('content')
       
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Infologin Media Production by <a href="http://itechmm.com">Itechmm.com</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/summernote.min.js') }}"></script>
    <!-- jQuery -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap-progressbar.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/nprogress.js') }}"></script>

    <!-- NProgress -->
   
  
    <!-- Custom Theme Scripts -->
    <script src="{{ asset('js/custom.min.js') }}"></script>
<script type="text/javascript">
   
   $(document).ready(function() {
    // Define function to open filemanager window
    var lfm = function(options, cb) {
        var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
        window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
        window.SetUrl = cb;
    };
    
    // Define LFM summernote button
    var LFMButton = function(context) {
        var ui = $.summernote.ui;
        var button = ui.button({
            contents: '<i class="note-icon-picture"></i> ',
            tooltip: 'Insert image with filemanager',
            click: function() {
      
                lfm({type: 'image', prefix: '/laravel-filemanager'}, function(url, path) {
                    context.invoke('insertImage', url);
                });

            }
        });
        return button.render();
    };

  $('#summernote').summernote({
     toolbar: [
            
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['view', ['fullscreen', 'codeview']],
            ['popovers', ['lfm']],
            ['table',['table']],
          ],
        buttons: {
            lfm: LFMButton
        }
  });
});
 
  </script>
    <script type="text/javascript">
        $(document).ready(function(){
          $('.removed').click(function(){
            var conf = confirm('Are you sure delete');
              if(conf){
                return true;
              }else{
                return false;
              }
          });
        });

    </script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script type="text/javascript">
  $('#lfm').filemanager('image');
</script>
 @yield('footerscript')
  </body>
</html>
