
<html lang="en" prefix="op: http://media.facebook.com/op#">
    <head>
        <meta charset="utf-8">
        <meta property="op:markup_version" content="v1.0">
        <meta property="fb:article_style" content="default"/>
        <link rel="canonical" href="{{ route('app.blog.view', ['slug' => $blog->new_id]) }}">
        <title>{{ $blog->new_title }}</title>
    </head>

<body>
    
    <article>
        <header>
            <h1>{{  $blog->new_title  }}</h1>
           
            <h3 class="op-kicker">
                {{$blog->category_name}}
            </h3>
            <address>
                Inarr Journal<!-- Replace with your author name-->
            </address>
            <time class="op-published" dateTime="$blog->published_at->format('c') }}">{{ $blog->published_at->format('M d Y, h:i a') }}</time>
            <time class="op-modified" dateTime="{{ $blog->updated_at->format('c') }}">{{ $blog->updated_at->format('M d Y, h:i a') }}</time>
        </header>
        <figure>
        <img src="http://inarr-journal.com{{$blog->feature_image}}" />
        <figcaption>{{$blog->category_name}}</figcaption>
      </figure>
        {{ strip_tags($blog->new_description) }}

        <footer>
        <aside>
           
        </aside>
        <small>© Copyright {{ date('Y') }}</small>
        </footer>
    </article>
    </body>
</html>