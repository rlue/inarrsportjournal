				<div class="side_bar_news">
					<section class="side_lastest_news">
						<h3>Latest News</h3>
					</section>
						
					<?php $i = 1; ?>
					@foreach($getnew as $new)
						<div class="row side_latest_new_title">
							<div class="news_img" id="newimg_<?php echo $i; ?>">
							<a href="{{route('detail',$new->new_id)}}">
								<img src="{{$new->feature_image}}" class="news_logo">	
							</a>
							</div>
							
							<div class="news_title" id="newtitle_<?php echo $i; ?>">
								<p><a href="{{route('detail',$new->new_id)}}" class="main_title">{{$new->new_title}}</a></p>
							</div>
						</div>
						<?php $i++; ?>
					@endforeach
						
				</div>

				<div class="row">
					<div class="side_ads">
					@foreach($bannerleft as $left)
						<a href="{{$left->ads_url}}" target="_black">
						<img src="{{$left->ads_image}}" class="side_ads_img">
						</a>
					@endforeach
					</div>
				</div>

					@include('nav.standing')