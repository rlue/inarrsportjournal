<div class="breaking_row navbar-fixed-top">
		<div class="container marquee_container">
			<div class="col-sm-12 marquee_col">
				<div class="breaking_news">BREAKING NEWS   |  <marquee class="marquee_row">
				@foreach($breakingnew as $breaking)
					<a class="marquee_link" href="{{route('detail',$breaking->new_id)}}"> <span class="fa fa-angle-double-right"></span> {{$breaking->new_title}} </a>
				@endforeach

				</marquee></div>
			</div>
		</div>
	</div>