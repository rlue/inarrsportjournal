	<div class="logo_row">
	<div class="row">
		<div class="col-sm-4">
			<a href="{{route('home')}}"><img src="{{ asset('images/logo_new.jpg') }}" class="logo_img"></a>
		</div>
		<div class="col-sm-8">

		@if(!$adstop->isEmpty())	
			@foreach($adstop as $top)
			<a href="{{$top->ads_url}}" target="_black"><img src="{{$top->ads_image}}" class="logo_ads"></a>
			@endforeach
		@endif
		</div>
	</div>
</div>
   <div class="row">
	   <div class="col-sm-12">
			<!-- Menu -->
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      	<a href="#" class="navbar-brand">
	               		Menu
	                </a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li  class="@if(Request::segment(1) != 'news' AND Request::segment(1) != 'articles' AND Request::segment(1) != 'forecast' AND Request::segment(1) != 'results' AND Request::segment(1) != 'worldcup' AND Request::segment(1) != 'contactup' AND Request::segment(1) != 'myanmarnews' ) active @endif"><a href="{{route('home')}}">HOME <span class="sr-only">(current)</span></a></li>
			        <li  class="@if(Request::segment(1) == 'news'
			        ) active @endif"><a href="{{route('news')}}">NEWS</a></li>
			        <li  class="@if(Request::segment(1) == 'articles' ) active @endif"><a href="{{route('articles')}}">ARTICLES</a></li>
			        <li  class="@if(Request::segment(1) == 'forecast') active @endif"><a href="{{route('forecast')}}">FORECAST</a></li>
			        <li  class="@if(Request::segment(1) == 'results' ) active @endif"><a href="{{route('results')}}">RESULTS</a></li>
			        <li  class="@if(Request::segment(1) == 'myanmarnews') active @endif"><a href="{{route('myanmarnews')}}">MYANMAR NEWS</a></li>
			         <li  class="@if(Request::segment(1) == 'worldcup') active @endif"><a href="{{route('worldcup')}}">WORLD CUP</a></li>
			        <li><a href="{{route('contactus')}}">CONTACT US</a></li>
			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
			<!-- logo and Menu -->
		</div>
	</div>