<div class="row">
						<div class="side_standing">
							<p class="standing">Standings</p>
							<ul class="standing_list">
								<li><a href="{{route('premierlanguage')}}">Premier League</a></li>
								<li><a href="{{route('spainlaliga')}}">La Liga</a></li>
								<li><a href="{{route('serieA')}}">Serie A</a></li>
								<li><a href="{{route('bundesliga')}}">Bundesliga</a></li>
								<li><a href="{{route('ligueOne')}}">France Ligue 1</a></li>
								<li><a href="{{route('championship')}}">League Championship</a></li>
							</ul>
						</div>
						<div class="side_standing">
							<a class="topscorers_link" href="{{route('topscorers')}}"><p class="standing">Top Scorers</p></a>
						</div>
					</div>